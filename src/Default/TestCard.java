package Default;


import java.util.Scanner;

public class TestCard {
    public static void main(String[] args) {
    Scanner reader = new Scanner(System.in);
    System.out.print("Enter the card notation: ");
    String notation = reader.next();
    Card card1 = new Card(notation);
    System.out.println(card1.getDescription());


    }

}

/**
 This is a test for the Card class, which outputs the full
 description of a deck of cards.
 **/
/**
public class TestCard
{
    public static void main(String[] args)
    {
        Scanner in = new Scanner(System.in);


        System.out.println("Enter the card notation:");
        String input = in.nextLine();
        Card card1 = new Card(input);
        System.out.println(card1.getDescription());
    }
}
 **/